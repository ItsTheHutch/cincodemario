<?php
session_start();
  require($_SERVER['DOCUMENT_ROOT'] . '/dbLogin.php');
  require($_SERVER['DOCUMENT_ROOT'] ."/sql/dbFunctions.php");
  require($_SERVER['DOCUMENT_ROOT'] ."/donation/convertCharacters.php");
  require($_SERVER['DOCUMENT_ROOT'] ."/donation/incentives.php");
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
        <link rel="stylesheet" href="/css/streamerDashboard.css">
        <meta http-equiv="refresh" content="60">
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      </div>
    </nav>

    <div class="">

      <div class="">

        <?php
        if(isset($_SESSION['lastTotal'])){
            $previousTotal=$_SESSION['lastTotal'];
        }
        else{
            $previousTotal=0.00;
        }        

        $currentTotal = getDonationsTotal();

        if($currentTotal > $previousTotal){
          echo "<div class='alert alert-success' role='alert'><center><h1>NEW DONATION RECEIVED!</h1></center></div>";
          $_SESSION['lastTotal'] = $currentTotal;
        }
        
        ?>
        
        
        <div class="dashboard-donation-total">
          <center>            
          <h2>Direct Relief Total</h2>
          <h1>$<?php echo $currentTotal; ?></h1>
          </center>
        </div>

        <?php
        date_default_timezone_set('America/New_York');
        $currenttime = date('h:i:s:u');
        list($hrs,$mins,$secs,$msecs) = explode(':',$currenttime);
        
        ?>

        <center><p>This page updates every minute. Last update at <?php echo "$hrs:$mins ".date('A'); ?> ET</p></center>


      <?php  

        getDonations();
      
      ?>

      </div>

    </div><!-- /container -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
