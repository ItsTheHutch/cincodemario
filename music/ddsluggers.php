<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/ddsluggerslive.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 20%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>D&ampD SLUGGERS</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/videoseries?list=PLjHkHamQfxKtbQrmj9q57U8ZPDtNgpc74"></iframe>
                                </div>
                        </div>

                        
                            <div class='music-social-media'>
                                <center>                      
                                    <a href='http://ddsluggers.com'><img src="/img/Website.png"></a>
                                    <a href='http://ddsluggers.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                    <a href='http://facebook.com/ddsluggers/'><img src="/img/facebookband.png"></a>
                                    <a href='http://twitter.com/ddsluggers/'><img src="/img/TwitterBand.png"></a>        
                                    <a href='http://youtube.com/user/danddsluggers'><img src="/img/YouTubeBand.png"></a>    
                                </center>
                            </div>
                        

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>D&D Sluggers is the solo venture of Tim White, a musician that seamlessly blends electronic instrumentation with indie-rock inspired guitar 
                                work and pop-punk driven melodies.</p>  
                            <p>(The) songs are built at their core through a Nintendo DS and fleshed out with White's infectious vocal melodies and simplistic yet rhythmically 
                                dense guitar work. Through a combination of loops and organic instrumentation, D&D Sluggers build brilliant and quirky tracks with minimal 
                                arrangements, but they're liable to get you dancing along in no time flat.</p>
                            <p>Tim is also a member of Half Empty Energy Tank and streams on Tuesday nights!</p>
                        </article>

                        

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
