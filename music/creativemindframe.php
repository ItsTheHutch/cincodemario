<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url("/img/music/creativemindframe.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 30%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>CREATIVE MIND FRAME (AKA 1-UP)</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/LHsCgodNpm4"></iframe>
                                </div>
                        </div>
                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://www.soundcloud.com/creativemindframe'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/creativemindframe'><img src="/img/facebookband.png"></a>                                      
                                <a href='http://youtube.com/user/creativemindframe'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>
                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Creative Mind Frame (Aka 1-UP) is a the best Ghanian business process engineer rapping nerd that plays the sax. He primarily fuses hip hop and jazz together focused on video games, anime and educational topics.</p>
                            <P>He also really loves Power Rangers.</p>
                            <br>
                            <p><a href='http://8bitx.com'>Photo by Rob Swackhamer - 8BITX.com</a></p> 
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
