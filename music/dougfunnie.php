<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45) ), url("/img/music/dougfunnie.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 0% 15%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>DOUG FUNNIE</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/285156821&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://dougfunnie.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/funniemusic'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/doughiphop/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/FunnieMusic/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/FunnieMusic'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Doug Funnie, Hip-Hop artist is described as “Indie-rocking funk and R&ampB with hopelessly erudite lyrics 
                            about pop culture, history, and science" when performing his music, known as Nerdcore. Nerdcore is a genre 
                            of hip-hop that concerns itself with, well, nerdy things. Video games, computers, comic books, animé and 
                            apparently (at least in Funnie’s case) professional wrestling. Doug's earlier works includes video game 
                            references works like "Metal Gear Solid" and "Wreck-it Ralph". Jason Phillips was born in Fort Ord, California 
                            of Panamanian and African American Descent. The military brat, now based in Dallas Texas, has been performing 
                            locally for 5+ years now.</p>
                            <p>Doug Funnie's newest release "Homecoming" is a larger-than-life yarn about Funnie running across the country 
                            to shake some cartoonishly sinister cops, The performers –– guest artists include Crunk Witch, Mike Lackey, 
                            Professor Shyguy, Richie Branson, and nerdcore giant MC Frontalot –– all wear outrageous guises, and things get 
                            silly, but it’s all in the service of a larger message, one that’s hidden, albeit only partially, behind a curtain.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
