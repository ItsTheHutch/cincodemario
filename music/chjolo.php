<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/CHJOLO.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 0% 45%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>CHJOLO</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/270532553&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                                </div>
                        </div>
                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://chjolo.com'><img src="/img/Website.png"></a>
                                <a href='http://chjolo.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/chjolo'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/chjolo.music'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/chjolo'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/channel/UCrkBDPDbVa3-6ju7l3qE1pg'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>
                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>With a background in music performance, soundtrack composition, and video game development, chjolo 
                                (aka Chris Logsdon) creates a wide variety of cult classic video game remixes and high-concept originals. 
                                His music combines the power of electronic music, the evocative soundscapes of game soundtracks, and the 
                                emotional energy of live-recorded instrumentation into an experience that keeps you on the edge of your seat!</p>
                            <p>As a member of the video game remix label GameChops, chjolo has produced remixes of Splatoon, Cave Story, 
                                Final Fantasy, and Smooth McGroove’s rendition of Xenogears! His tunes have been heard at conventions such as 
                                PAX East, MAGFest, and SGC/RTX!</p>
                            <p>Every week, chjolo hosts a live performance on streaming sites like Twitch and Beam; he has even developed 
                                his own viewer interaction interface to generate an all-new brand of audience participation!</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
