<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/Mykah.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 0% 55%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>MYKAH</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/videoseries?list=PLdnU7aJC-M8iSR5B-hsjrfq48gyvvps4_"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://mykah.net'><img src="/img/Website.png"></a>
                                <a href='http://mykah.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/mykah'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/djmykah/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/djmykah/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/DJMykah'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Mykah is an electronic dance music producer, remixer and DJ from the UK currently 
                                living in London. Mykah specialises in making heavy club banger remixes of classic 
                                videogame music and original electronic dance music in a whole host of genres from 
                                house to dubstep to drum &amp bass.  Mykah draws his inspiration from the classic 
                                Nintendo and Sega soundtracks of the 90s combined with the music of the UK's underground 
                                dance music scene. He has been producing and DJing since 2004.  One of the first 
                                artists on the GameChops videogame dance music label Mykah has released remixes of 
                                The Legend of Zelda, Final Fantasy, Kirby, Bastion, Sonic, Splatoon, Undertale and 
                                more. Mykah has also performed internationally at events in the UK, USA and Japan.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
