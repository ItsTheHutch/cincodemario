<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.15) ), url("/img/music/megaran.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 10%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>MEGA RAN</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CPEa8mWxYkQ"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://megaran.com'><img src="/img/Website.png"></a>
                                <a href='http://megaranmusic.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/megaranmusic'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/MegaRanMusic/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/megaran/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/megaranmusic'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p><i>"Mega Ran's proven he can excel in storytelling, punchlines, poignancy, freestyling and, well, pretty much any other way you can measure an MC." - LA Weekly</i></p>
                            <p>Mega Ran, aka Raheem Jarbo of Philadelphia is arguably the world's most well known video-game influenced performance artist. Since signing an 
                            unprecedented licensing deal with international developer Capcom, the former teacher's high-energy shows and fun, nerdy persona have led to world 
                            tours, festival and video game tournament performances, and a legion of smart art fans he calls "Team Mega."</p>
                            <p>Mega Ran's newest release “RNDM” debuted at #2 on the Billboard Heatseekers Chart, and ended the year 11th on CMJ's Year End Hip Hop Chart, 
                            above Dr. Dre's Compton, and several other notable releases. </p>
                            <p>Ran continues to bend genre definitions and reality with each release, through chiptunes, hip-hop and beyond. Mega Ran may have traded in 
                            the chalk for the microphone, but every song, every lyric, and every appearance is a lesson. School is in.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
