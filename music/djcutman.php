<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/djcutman.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 0% 20%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>DjCUTMAN</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/35927333" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://djcutman.com'><img src="/img/Website.png"></a>
                                <a href='http://djcutman.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/DjCUTMAN'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/DjCUTMAN/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/VideoGameDJ/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/GameChops'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>After casting off the shackles of Dr. Wily, CUTMAN was reborn to rock faces and lift hearts. Even the coldest robot masters can't help but move to the razor 
                                sharp sounds of this maverick turned hero. Dj CUTMAN approaches the stage with a mission: To bring the very best video game music and chiptunes to your ears. 
                                With relentless dedication, CUTMAN runs a record label, blog, and a weekly chiptune show.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
