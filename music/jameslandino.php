<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/jameslandino.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 0% 45%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>JAMES LANDINO</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/277285182&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                                <center>                      
                                    <a href='http://jameslandino.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                    <a href='http://www.soundcloud.com/jameslandino'><img src="/img/SoundCloud.png"></a>
                                    <a href='http://facebook.com/jameslandinomusic/'><img src="/img/facebookband.png"></a>
                                    <a href='http://twitter.com/jameslandino/'><img src="/img/TwitterBand.png"></a>        
                                    <a href='http://youtube.com/user/jameslandino'><img src="/img/YouTubeBand.png"></a>    
                                </center>
                            </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Teleporting in from the game studio that brought you ROCK BAND, music composer and DJ James Landino 
                            is a leading force in video game music. When he's not writing music for Harmonix's next big title, James 
                            entertains audiences by remixing your favorite game soundtracks at live venues across the United States.</p>
                            <p>Most recently, James wrote original music for AMPLITUDE, the Sony Playstation 4 reboot of the original PS2 classic that inspired the music rhythm game genre. His additional credits include: ROCK BAND 4, KINGDOM HEARTS 2.5 HD REMIX, SONIC THE HEDGEHOG: AFTER THE SEQUEL and the hit anime series RWBY.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
