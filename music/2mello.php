<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55) ), url("/img/music/2mello.png"); 
                            background-repeat: no-repeat;
                            background-position: 100% 30%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>2 MELLO</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ax3H0o5ljhc"></iframe>
                                </div>
                        </div>
                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://2mello.net'><img src="/img/Website.png"></a>
                                <a href='http://2mellomakes.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/2mello'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/2mellomakes/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/mellomakes/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/2mello'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>
                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>2 Mello is a Hip Hop producer and emcee, heavily influenced by the soul music of his own life: A Tribe Called Quest and their enlightened east 
                            coast jazzy Hip Hop, and the achingly beautiful electronic sample-based symphonies of Bristol players Massive Attack, DJ Shadow and Portishead from across the pond.</p>
                            <p>He began producing on computers at age 14, after four years of musical training on piano and clarinet. He still tries to make a beat a day. He has spent his entire 
                            life in in Lexington, Kentucky, a city whose wider community does not support unique Hip Hop, but the Internet is helping him gain a wider audience every hour. His 
                            first big break came when he became an artist of Scrub Club Records after talking with fellow artist and label head MadHatter and realizing they shared the same vision 
                            of what music should be.</p>
                            <p>2 Mello champions personality above all else in Hip Hop, which is very apparent on his debut album, "Super Producer." Mello is geeky and proud of it, describing his 
                            childhood as being spent in lonesome bliss playing Super Nintendo games, watching dark science fiction films and devouring novels far above his reading level to gain 
                            ideas for his own writing. 2 Mello’s artistic goal is to relate his obsessions with music and geek culture to his damaged connection with the real world and all its 
                            beauties and perils. He continued this with his two critically-acclaimed mashup albums, "Chrono Jigga" and "Nastlevania".</p>
                            <p>He holds a Bachelor’s Degree in English Literature and is self-employed as a video director and writer.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
