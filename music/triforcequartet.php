<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/triforcequartet.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 10%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>TRIFORCE QUARTET</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/NGtf6F59jTQ"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://triforcequartet.com/'><img src="/img/Website.png"></a>
                                <a href='http://facebook.com/TriforceQuartet/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/TriforceQuartet/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/shwrtzify'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>The Triforce Quartet is a traditional string quartet that plays video game music.</p>
                            <p>The Triforce Quartet began innocently as a short Zelda medley for an encore of a recital in early 2007, but 
                                the video of using traditional string quartet instruments to play music of classic games gained viral popularity 
                                later that summer. Thanks to this, cellist Chad Schwartz was able to combine his love for video games, along with 
                                years of classical training, to arrange a variety of video game themes that even non-gamers can enjoy. The four 
                                members of the Triforce Quartet take audience members on an unforgettable musical journey.</p>
                            <p>The Triforce Quartet has played in front of sold out shows at PAX Prime in Seattle, PAX East in Boston; MAGFest in 
                                Washington DC, the iDIG Music Festival in Ireland and with Video Games Live!</p>                            
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
