<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/kadeshflow.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 45%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>KADESH FLOW</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aQHlnL-3aLc"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://kadeshflow.com'><img src="/img/Website.png"></a>
                                <a href='http://kadeshflow.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/kadeshflow'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/KadeshFlow/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/KadeshFLow/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/KadeshFlow'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Ryan "Kadesh Flow" Davis brings realism, consciousness and otaku thoughts together in riveting flow format. 
                                He has composed original music for Toonami Asia and for FUNimation backed documentaries such as "The One Piece 
                                Podcast Goes to Japan", performed live at the Cannes Film Festival, shared the stage with the likes of Caroline 
                                Shines, The Revivalists, B.o.B., CBDB, and others. He has been publicly lauded by  outlets such IGN and Kotaku, 
                                and his music has been featured on network television in multiple countries and territories across Southeast Asia.</p>
                            <p>As a rapper, trombonist, and producer, Kadesh has been wowing both suspecting and unsuspecting listeners with his 
                                musical versatility since his early teens. At age 11, 'Desh, as many of his listeners call him, began rapping and 
                                playing trombone within two weeks of one another. Now, the young emcee actively blends his jazz and hip hop backgrounds 
                                into body bouncing and head bobbing tracks that range from crunk to sultry, from melodic to face-melting. Kadesh's wide 
                                reaching and relatable content consistently wins over harsh hip hop critics with his lyrical dexterity, drawing from 
                                multiple influences, including his collegiate experience, socio-cultural musings, spirituality, and anime and video game 
                                related passions. He has collaborated with the likes of Steve Blum (the voice of T.O.M.) and indie hip hop staples such 
                                as Mega Ran and Richie Branson. Currently he is lighting up stages around the country, gracing such events as Official 
                                SXSW Showcases, MAGFest, PAX Prime, FanExpo: Dallas, Anime Expo &amp Otakon, as well as numerous music venues throughout 
                                the country.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
