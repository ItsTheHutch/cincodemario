<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55) ), url("/img/music/bitbrigade.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 40%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>BIT BRIGADE</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UkUSIS_jHuI"></iframe>
                                </div>
                        </div>
                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://bitbrigade.com'><img src="/img/Website.png"></a>
                                <a href='http://bitbrigade.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://facebook.com/bitbrigade/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/bit_brigade/'><img src="/img/TwitterBand.png"></a>        
                            </center>
                        </div>
                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>When Bit Brigade rolls into town, the gamer elite hang up the controller for the evening and see a rock 
                                show. With unprecedented attention to detail and post-rock bombast, Bit Brigade meticulously replicates 
                                every musical cue, cutscene and boss battle in perfect syncronization with master gamer Noah McCarthy's 
                                inspiring speed-trial run of each level. Composed of members of roadwarrior (both stateside and abroad) 
                                mathrock bands Cinemechanica and We Versus The Shark, Bit Brigade elevates game music to its proper place 
                                in the foreground of epic technical rock and plays the games like they don't need the extra lives. Which, 
                                for the record, they don't.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
