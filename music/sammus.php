<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/sammus.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 20%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>S▲MMUS</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/s64aSHOk3bg"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://sammusmusic.com'><img src="/img/Website.png"></a>
                                <a href='http://sammusmusic.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/sammusmusic'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/sammusmusic/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/sammusmusic/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/sammusmusic'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>SAMMUS (Enongo Lumumba-Kasongo) is an Ithaca, NY based rap artist, producer, and PhD student in the 
                                Department of Science & Technology Studies at Cornell University.  Known as much for her rousing 
                                stage presence as she is for her prowess as a beatmaker and lyricist, Sammus has spent the past 
                                several years cultivating a strong following of activists, hip hop heads, punks, and self-proclaimed 
                                nerds and geeks, among others. As noted by the Los Angeles Times, Sammus “has a gift for getting a 
                                message across.” Now ready to make her Don Giovanni debut (while remaining tied to NuBlack Music Group), 
                                she is poised to cement herself as an artist who consistently thinks outside boxes and dances across 
                                lines (and does other neat things with geometrical figures).</p>
                        </article>

                    </div>

                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
