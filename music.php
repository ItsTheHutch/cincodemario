<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include(__DIR__ . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include(__DIR__ . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include(__DIR__ . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45) ), url("/img/ddrmario.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 40%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>LIVE MUSIC</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content">
                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'>            
                            </article>
                        </div>
                    </div>
                    <center>
                            <ul class="music-thumbs">       
                            <li><a href="/music/professorshyguy.php"><img src="img/music/thumbnails/ProfessorShyGuy.png" alt="Professor ShyGuy"></a></li>
                            <li><a href="/music/benbriggs.php"><img src="img/music/thumbnails/BenBriggs.png" alt="Ben Briggs"></a></li>                                                        
                            </ul>
                    </center>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>    

                    
                     


                </div>   



            </div><!--end top-half-->
            
            <?php include(__DIR__ . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include(__DIR__ . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
