<footer>
    <ul>
        <li id="footer-logo"><a href="http://halfemptyetank.com"><img class="hidden-xs hidden-sm" src="/img/HEETlogo.png"><img class="hidden-md hidden-lg" src="/img/heettank.png"></a></li>
        <li id="footer-icon"><a href="http://facebook.com/halfemptyenergytank"><img src="/img/FacebookYellow.png"></a></li>
        <li id="footer-icon"><a href="http://twitter.com/halfemptyetank"><img src="/img/TwitterYellow.png"></a></li>
        <li id="footer-icon"><a href="http://youtube.com/halfemptyenergytank"><img src="/img/YouTubeYellow.png"></a></li>
        <li id="footer-icon"><a href="http://HEET.tv"><img src="/img/TwitchYellow.png"></a></li>
    </ul>
    <br>
    <p>&#xA9; <?php echo date("Y");?> Half Empty E-Tank</p>
</footer>