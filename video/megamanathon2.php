<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url("/img/video/mm2-imagebar.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 45%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>MEGA MAN-ATHON 2 VIDEOS</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end image-bar-->

                <div class="main-content">
                    <div class="adjust-table container-fluid">                   
                    <center><h3>Please note that due to network issues, a number of games are broken up into mulitple videos.</h3></center>
                    <br>
                    <div class="row">
                        <div class="col-large-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-striped table-responsive">
                                <tr class="">
                                    <td><b>Video</b></td>
                                    <td><b>Player</b></td>
                                    <td><b>Video Length</b></td>
                                    <td><b>Details</b></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=z5FFDq2cnwE&t=94s">Mega Man-athon 2 Promo</a></td>
                                    <td></td>
                                    <td>3 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46181738">Mega Man</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>43 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46181674">Mega Man 2</a></td>
                                    <td>Noah McCarthy</td>
                                    <td>48 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46181650">Mega Man 3</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 4 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46187556">Mega Man 4</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 2 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46186502">Mega Man 5</a></td>
                                    <td>ItsTheHutch</td>
                                    <td>2 hr 3 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46186600">Mega Man 6</a></td>
                                    <td>Andy of The X-Hunters</td>
                                    <td>1 hr 4 min</td>
                                    <td>Stuttering Craig of Screwattack stops by!</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46194231">Mega Man 7</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 41 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46194472">Mega Man 8</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 46 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46194203">Mega Man 9</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 12 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46194246">Mega Man 10</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 5 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46194073">Mega Man Battle Network</a></td>
                                    <td>PrismaticBlack</td>
                                    <td>3 hr 12 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46194344">Mega Man Battle Network 3 Blue</a></td>
                                    <td>Gamer0278</td>
                                    <td>4 hr 14 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46244280">Rockman.EXE WS</a></td>
                                    <td>Method1cal</td>
                                    <td>50 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46244468">Mega Man X</a></td>
                                    <td>Brendan Blakemore</td>
                                    <td>56 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46244274">Mega Man X3</a></td>
                                    <td>SlappyMeats</td>
                                    <td>1 hr 46 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man Unlimited - Part <a href="https://www.twitch.tv/halfemptyenergytank/v/46244969">1</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/46354499">2</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 58 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46354565">Mega Man Rock Force</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 37 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46354789">Mega Man Atari Demake</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>1 hr 35 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46354184">Mega Man: Dr. Wily's Final Attack</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>49 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46354526">Mega Man: Dr. Wily's Revenge</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>30 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46355116">Mega Man II (GameBoy)</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>32 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46354626">Mega Man III (GameBoy)</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>42 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46406548">Mega Man IV (GameBoy)</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 12 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46406572">Mega Man V (GameBoy)</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 5 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46406893">Mega Man X7</a></td>
                                    <td>Masterjoe116</td>
                                    <td>3 hr</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46406879">Mega Man X</a></td>
                                    <td>HEET</td>
                                    <td>1 hr 18 min</td>
                                    <td>Volunteer Run</td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man Battle Network 4 Red Sun - Part <a href="https://www.twitch.tv/halfemptyenergytank/v/46407128">1</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/46407018">2</a></td>
                                    <td>Method1cal</td>
                                    <td>6 hr 19 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46407172">Mega Man X2</a></td>
                                    <td>Brendan Blakemore</td>
                                    <td>1 hr 18 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46406950">Mega Man Star Force Pegasus</a></td>
                                    <td>Method1cal</td>
                                    <td>4 hr 55 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46407170">Mega Man Battle Network Chrono X</a></td>
                                    <td>Tterraj42</td>
                                    <td>1 hr 21 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46408382">Mega Man Zero</a></td>
                                    <td>Tterraj42</td>
                                    <td>2 hr 43 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46410354">Mega Man Zero 2</a></td>
                                    <td>Tterraj42</td>
                                    <td>2 hr 5 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46428942">Mega Man Battle Network 6: Cybeast Gregar</a></td>
                                    <td>Method1cal</td>
                                    <td>2 hr 54 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46428889">Mega Man X4</a></td>
                                    <td>Jirard Khalil</td>
                                    <td>1 hr 42 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46429170">Mega Man Legends</a></td>
                                    <td>Brittany Saturn</td>
                                    <td>1 hr 55 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46429139">Mega Man Soccer</a></td>
                                    <td>HEET</td>
                                    <td>43 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46429072">Mega Man X + Marathon End</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>1 hr 9 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46429769">Pixelitus.net Interview</a></td>
                                    <td></td>
                                    <td>9 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/46429322">The Megas Interview</a></td>
                                    <td></td>
                                    <td>7 min</td>
                                    <td></td>
                                </tr>
                            </table>
                        </div><!-- end col div -->
                    </div><!-- end row div -->
                </div><!-- end container-fluid div -->



                



            </div><!--end top-half-->
            
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
