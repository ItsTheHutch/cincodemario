<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url("/img/video/mm-imagebar.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 35%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action container-fluid">
                        <h1>MEGA MAN-ATHON...VIDEO?</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end image-bar-->

                <div class="main-content">                    
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2">
                                <div class="video-page-selection">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/B1PgK-jszPA"></iframe>
                                    </div>
                                    <br>
                                    <p>Rumor has it that video footage of this event exists...somewhere...</p>
                                    <br>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div><!--end main-content-->

                



            </div><!--end top-half-->
            
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
