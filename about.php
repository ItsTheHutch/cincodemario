<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include(__DIR__ . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include(__DIR__ . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include(__DIR__ . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("/img/aboutheader.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 35%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>ABOUT MEGA MAN-ATHON</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content">
                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>
                                <h1><b>In the year 20XX, a team of super-fighting robots join forces with one mission: to improve the lives of children and defeat 
                                    Dr. Wily once and for all...</b></h1>

                                <p>For 72 hours, this team will use their unique abilities, speed and tenacity to siege Wily’s Castle.<p>
                                
                                <p><b>This is the Mega Man-athon--an annual, non-stop gauntlet of Mega Man, Mega Man-inspired games and music to raise money for 
                                    Child’s Play charity--presented by MAGFest!</b></p>                
                                
                                <p>Each year, as the Mega Man-athon team returns to take on more of Wily’s Robot Masters, they are joined by new allies: guest 
                                    players, speedrunners, game developers, YouTubers and more! Each event offers nightly live musical performances, featuring 
                                    some of the most diverse, talented acts the VGM scene has to offer.</p>

                                <p>Even you can join the fight! Donate to the Mega Man-athon to help the team to victory. Your donation might even win you a prize!</p>

                                <p>100% of proceeds donated to the Mega Man-athon go to Child’s Play. Child's Play is a charity that raises money to provide games, 
                                    peripherals, consoles and more to children's hospitals, therapy centers and domestic violence shelters. You might even say 
                                    Child’s Play helps children GET EQUIPPED with valuable resources to provide them comfort, entertainment and healthy distraction 
                                    as they fight their own battles.</p>

                                <p>Child’s Play is a well-established charity organization in the video game community. An astounding 95% of all donations to Child’s Play 
                                    go directly to benefit the hospitals (only 5% administrative overhead). You can learn more about Child's Play at 
                                    <a href="http://childsplaycharity.org">childsplaycharity.org</a>.

                                <p>The Mega Man-athon is hosted by <a href="http://halfemptyetank.com">Half Empty Energy Tank</a> (HEET).  The event is held every 
                                year at the Gaylord National Resort in National Harbor, MD during Super MAGFest and is streamed live on Twitch and 
                                <a href="http://megamanathon.tv">megamanathon.tv</a> for the full 72 hours of the event.</p>
                                <br>
                                <hr>
                                <br>      


                                <center>
                                <img src="/img/MegaManathon01logo.png">
                                <h2>January 3-6 2013</h2>
                                </center>        
                                <p>In the year 2013, one man conceived a plan to fight Dr. Wily, but he could not do it alone! He gathered the members of Half Empty 
                                    Energy Tank together and put his plan into motion: the first ever Mega Man-athon.</p>
                                <p>Held at MAGFest 11 in the console room, the first Mega Man-athon began with the goal of playing as many Mega Man games as possible 
                                    and raising money for Child's Play Charity. Special guests included: Stuttering Craig of Screwattack, Mega Ran, The Megas and The 
                                    Protomen!</p>
                                <p>The first Mega Man-athon was sponsored by Capcom as a part of the Mega Man 25th Anniversary celebration.</p>
                                <p>This event also commemorated Half Empty Energy Tank’s first ever live-streamed event—now one of countless broadcasts and events.</p>
                                <br>
                                <center><h2>$410 raised for Child's Play!</h2></center> 
                            </article>
                        </div>
                    </div>


                    <div id="mm1" >     
                            <style>
                                #mm1{
                                    background: url("/img/megamanathonset.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>

                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>   
                                <center>
                                <img src="/img/MegaManathon02logo.png">
                                <h2>January 2-5 2014</h2>
                                </center>          
                                <p>A year later, HEET brought the Mega Man-athon back to MAGFest! After a year of regular live-streaming, the team was better 
                                    prepared but also ready to take risks. Notable guests that took the time to play games included Andy of the X-Hunters, 
                                    Noah McCarthy of Bit Brigade and Jirard "The Completionist" Khalil!</p>
                                <br>
                                <center><h2>$1,190 raised for Child's Play!</h2></center>
                            </article>

                        </div>
                    </div>

                    <div id="mm2" >     
                            <style>
                                #mm2{
                                    background: url("/img/megamanathon2set.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>

                    

                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>
                                <center>   
                                <img src="/img/MegaManathon03logo.png">
                                <h2>January 23-26 2015</h2>
                                </center>          
                                <p>As the Mega Man-athon returned for a third year,  the stage expanded and moved from the console room to a corner in 
                                    front of the Expo Hall—a spot with fantastic views of the harbor and the Capital Wheel! The team welcomed numerous 
                                    notable guests at the Mega Man-athon 3, including: Jirard and Greg from That One Video Gamer, the Super Guitar Bros., 
                                    and Hot Pepper Gaming!</p>
                                <p>This was also the first year that the Mega Man-athon embraced the "music" part of the "Music and Gaming Festival." 
                                    Teaming up with Insert Coin Theater, D&ampD Sluggers, Mega Flare, Super Guitar Bros., 2 Mello and Sammus joined the 
                                    fight with amazing live performances.</p>
                                <br>
                                <center><h2>$4,103 raised for Child's Play!</h2></center>  
                            </article>
                        </div>
                    </div>

                    <div id="mm3" >     
                            <style>
                                #mm3{
                                    background:url("/img/megamanathon3set.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>                                


                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>
                                <center>   
                                <img src="/img/MegaManathon04logo.png">
                                <h2>February 18-21 2016</h2>
                                </center>          
                                <p>As another year passed, the Mega Man-athon grew even stronger! Along with a wide variety of Mega Man games representing 
                                    the whole franchise, as well as some fan-made games, the Mega Man-athon 4 spotlighted the new Mega Man board game, which 
                                    was a crowd favorite of the year. Live music returned to the Mega Man-athon stage in force, featuring 3 nightly music 
                                    blocks with performances from: Ben Briggs, Tetracase, Ralfington, Grimecraft, DjCUTMAN, 2 Mello, Shadow Clone, LucioPro, 
                                    D&ampD Sluggers, Mega Ran, Super Guitar Bros., Crunk Witch, The Blast Processors, Sammus and Professor Shyguy!</p>
                                
                                <p>The Mega Man-athon 4 was also the first event to feature non-Mega Man games: Shovel Knight and Azure Striker Gunvolt. Matt 
                                    Papa of Inti Creates, developers of the Mega Man Zero and Mega Man ZX series, and Gunvolt voice actress Megu Sakuragawa 
                                    took to the stage to showcase Azure Stiker Gunvolt.</p> 
                                
                                <p>The fourth annual Mega Man-athon was the biggest and most successful yet, raising a total of $5,200 for Child’s Play!</p>
                                <br>
                                <center><h2>$5,200 raised for Child's Play!</h2></center>  
                            </article>
                        </div>
                    </div>

                    <div id="mm4" >     
                            <style>
                                #mm4{
                                    background:url("/img/grimecraft.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>



                </div>



            </div><!--end top-half-->
            
            <?php include(__DIR__ . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include(__DIR__ . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
