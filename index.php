<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include(__DIR__ . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include(__DIR__ . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="main-content-full">

                    <?php include(__DIR__ . '/includes/header.php'); ?>

                    <?php 
                        $test=0; //set to 1 if testing page before event, 0 for production
                        $date = time();
                        $eventStart = strtotime('2018-05-04 5:00:00');

                        if($date > $eventStart || $test==1){

                    ?>

                    <div class="call-to-action container-fluid">

                                
                                <div class="row">
                                    <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-offset-3 col-lg-6 col-lg-offset-3">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="http://player.twitch.tv/?channel=halfemptyetank"></iframe>
                                    </div>
                                    </div>
                                </div>    
                                
                                <div class="transbox">
                                    <?php 
                                    include($_SERVER['DOCUMENT_ROOT'] . '/donation/incentives.php'); 
                                    $grandTotal = getGrandTotal();?>
                                    <h1>$<?php echo $grandTotal;?></h1>        
                                    <h2>raised for Direct Relief! <a href="/donation/donate.php">Donate Now!</a></h2>
                                </div>
                                
                    </div><!--end call-to-action-->

                    <?php 
                        }
                        else{ 
                    ?>


                    <div class="call-to-action container-fluid">

                                
                                <div class="row">
                                    <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-offset-3 col-lg-6 col-lg-offset-3">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/3UcMU8sOor0"></iframe>
                                    </div>
                                    </div>
                                </div>    
                                
                                <h1>May 4-6 2018</h1>        
                                <h2>48 Hours Celebrating Mario and Supporting Direct Relief!</h2>
                                <h2>Streamed live from <a href="http://CincoDeMar.io">CincoDeMar.io</a> and <a href="http://HEET.tv">HEET.tv</a>!</h2>                                                     
                                
                    </div><!--end call-to-action-->

                    <?php 
                        }
                    ?>

                    
                
                </div><!--end main-content-full-->


                <div class="charity-info container-fluid">
                    <div class="row media">                            
                                <!--<div class="media-left media-middle">
                                    <a href="#">
                                    <img class="media-object hidden-xs hidden-sm" src="/img/childsplay_logo.jpg" alt="Child's Play Charity">
                                    </a>
                                </div> Is not showing up for some reason???? -->
                                <div class="media-body media-middle">
                                    <center><img class="" src="/img/DirectRelief/drLogo.png" alt="Direct Relief"></center>
                                    <br>
                                    <div id="charity-image">                      
                                    </div>
                                    <br>
                                   <!-- <center><img class="" src="/img/DirectRelief/Photos/16.jpg" alt="Direct Relief"></center>-->
                                    <h2 class="media-heading"><b><a href="/donation/donate.php">Donate Now to Support Direct Relief!</a></b></h2>
                                    <p>Direct Relief is a humanitarian aid organization, active in all 50 states and 70 countries, with a mission to improve the health and lives of people affected by poverty or emergencies.</p>
                                    <p>Direct Relief’s assistance programs focus on maternal and child health, the prevention and treatment of disease, and emergency preparedness and response, and are tailored to the particular circumstances and needs of the world’s most vulnerable and at-risk populations.</p>
                                    <br>
                                    <p id="charity-link"><a href="http://DirectRelief.org/">Visit DirectRelief.org to learn more!</a></p>
                                </div><!-- end media-body div -->
                    </div><!-- end row -->
                </div><!-- end container-fluid -->

                <!--
                <div class="prizes container-fluid">
                <h2>Donate and you may win a prize!</h2>                        

                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h3>Minimum $5 Donation</h3>
                            </article>
                        </div>
                    </div>
                    <center>
                        <ul class="prize-thumbs">
                            <li><img src="img/prizes/SB3book.png" alt="Super Mario Bros. 3: Brick by Brick"><p>Super Mario Bros. 3: Brick by Brick</p><p>Autographed by Bob Chipman</p></li>
                            <li><img src="img/prizes/Amiibos.png" alt="Amiibo"><p>One Super Mario Series Amiibo</p></li>
                        </ul>
                    </center>
                            

                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h3>Minimum $10 Donation</h3>
                                <p>Also eligible for the $5 tier prizes!</p>
                            </article>                                
                        </div>
                    </div>
                    <center>
                        <ul class="prize-thumbs">
                            <li><img src="img/prizes/Coasters.png" alt="Super Mario Coasters"><p>Super Mario Coasters</p><p>Made by BrittanySaturn</p></li>
                        </ul>
                    </center>



                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h3><b>Grand Prize</b></h3>
                                <h3>Minimum $20 Donation</h3>
                                <h3>If picked, you win EVERYTHING below!</h3>
                            </article>                                
                        </div>
                    </div>
                    <center>
                        <ul class="prize-thumbs">
                            <li><img src="img/prizes/Coasters.png" alt="Super Mario Coasters"><p>Super Mario Coasters</p><p>Made by BrittanySaturn</p></li>
                            <li><img src="img/prizes/SB3book.png" alt="Super Mario Bros. 3: Brick by Brick"><p>Super Mario Bros. 3: Brick by Brick</p><p>Autographed by Bob Chipman</p></li>
                            <li><img src="img/prizes/GoldMario.png" alt="Amiibo"><p>Gold Mario Amiibo</p></li>
                            <li><img src="img/prizes/MarioKart8.png" alt="Amiibo"><p>Mario Kart 8 Deluxe</p></li>
                        </ul>
                    </center>   
                    <br>
                    <h4><a href="https://docs.google.com/spreadsheets/d/1vD1OJ8rvZuKxxlaxpLymY3pyJkSN9V_Om2Bwb2ZXHzI/edit#gid=0">No purchase necessary.  Click for prize rules.</a></h4>                

                </div> 
                -->


            </div><!--end top-half-->
            
            <?php include(__DIR__ . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include(__DIR__ . '/includes/bottomscripts.php'); ?>

    </body>
</html>
