<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include(__DIR__ . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include(__DIR__ . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include(__DIR__ . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("/img/aboutheader.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 35%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>SCHEDULE</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end image-bar-->

                <div class="main-content">
                    <div class="adjust-table container-fluid">
                    <br>
                    <center><h4>Since we're running this at MAGFest, the schedule always changes. We'll try to keep this up to date!</h4></center>
                    <br>
                    <div class="row">
                        <div class="col-large-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-striped table-responsive">
                                <tr>
                                    <td>Time (ET)</td>
                                    <td>Game</td>
                                    <td>Player</td>
                                    <td>Incentive</td>
                                </tr>
                                
                                <tr class="info">
                                  <td><h4>Thursday January 5 2017</h4></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                
                                <?php 
                                    require(__DIR__ . '/dbLogin.php');
    
                                    $stmt = "SELECT * FROM Schedule";
                                    
                                    try {
                                        $conn = new PDO($servername, $username, $password);

                                        // set the PDO error mode to exception
                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


                                        //prepare sql and bind parameters
                                        $stmt = $conn->prepare($stmt); 
                                        $stmt->execute();

                                        //$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                                        //file_put_contents('ipn.log', "result: ".$result. PHP_EOL, LOCK_EX | FILE_APPEND);
                                        $dayPointer = 1;

                                        while($row = $stmt->fetch()){
                                            //file_put_contents('ipn.log', "row: ".$row . PHP_EOL, LOCK_EX | FILE_APPEND);
                                            $time = $row["GameTime"];
                                            $game = $row["Game"];
                                            $player = $row["Player"];
                                            
                                            $day = date_parse($time);
                                            
                                            if($day["day"] == 6 && $dayPointer == 1){
                                                echo "<tr class='info'><td><h4>Friday January 6 2017</h4></td><td></td><td></td><td></td></tr>";
                                                $dayPointer = 2;
                                                                    
                                            }
                                            else if($day["day"] == 7 && $dayPointer == 2){
                                                echo "<tr class='info'><td><h4>Saturday January 7 2017</h4></td><td></td><td></td><td></td></tr>";
                                                $dayPointer = 3;
                                            }
                                            else if($day["day"] == 8 && $dayPointer == 2){
                                                echo "<tr class='info'><td><h4>Sunday January 8 2017</h4></td><td></td><td></td><td></td></tr>";
                                                $dayPointer = 4;
                                            }
                                            
                                            $date = new DateTime($time);
                                            
                                            echo"<tr><td>".$date->format('h:i a')."</td><td>".$game."</td><td>".$player."</td></tr>";
                                        }

                                    }
                                    catch(PDOException $e){
                                        echo "Error: " . $e->getMessage();
                                        file_put_contents('ipn.log', "caught error: " . PHP_EOL, LOCK_EX | FILE_APPEND); //TESTING DELETE LATER
                                    }           

                                    //End the database connection
                                    $conn = null;
                                
                                ?>
        
                            </table>
                        </div><!-- end col div -->
                    </div><!-- end row div -->
                </div><!-- end container-fluid div -->






                </div>



            </div><!--end top-half-->
            
            <?php include(__DIR__ . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include(__DIR__ . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
