<?php
    
               
    function getDonationsTotal(){
        require($_SERVER['DOCUMENT_ROOT'] . '/dbLogin.php');

        $sql = "SELECT TRUNCATE(SUM(Amount),2) FROM Transactions_2018";

        try {
            $conn = new PDO($servername, $username, $password);               
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
            
            //prepare sql
            $stmt = $conn->prepare($sql);                         
            $stmt->execute();
            $total = $stmt->fetch();
            $currentTotal = $total[0];

            if($currentTotal == NULL){
                $currentTotal = 0.00;
            }
            
            return $currentTotal;
                        
        }
        catch(PDOException $e){
            echo "Error: " . $e->getMessage();

        }           
        
        //End the database connection
        $conn = null;
        
    }






    /**
     *  Get the last 50 Direct Relief donations.    
     *
     *  @author Chris Hutcherson
     */

    function getDonations(){
        require($_SERVER['DOCUMENT_ROOT'] . '/dbLogin.php');

        try{
            $conn = new PDO($servername, $username, $password);               
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT ID, TimeOfDonation, Name, Amount, Incentive, Message FROM Transactions_2018 ORDER BY ID DESC LIMIT 50");            

            $stmt->execute();
            echo "<div class='row'>
                    <div class='col-large-12 col-md-12 col-sm-12 col-xs-12'>
                    <table class='table table-striped table-responsive'>
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Time</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Incentive</th>
                    <th>Message<th>
                    </tr>
                    </thead>";
            echo "<tbody>";

            while($row = $stmt->fetch()){          
                $id = $row["ID"]; 
                $timestamp = date_create($row["TimeOfDonation"]);  
                $name = convertCharacters($row["Name"]);                
                $amount = convertCharacters($row["Amount"]); 
                $incentive = convertCharacters($row["Incentive"]);
                $message = convertCharacters($row["Message"]);

                echo "<tr><td>".$id."</td><td>".date_format($timestamp,'Y-m-d g:i A')."</td><td>".$name."</td><td>$".$amount."</td><td>".$incentive."</td><td><div class='donationMessage'>".$message."</div></td></tr>";
            }  

             echo "</tbody></table></div></div>";
            
            
        }
        catch(PDOException $e){
            echo "Error: " . $e->getMessage();

        }           
        
        //End the database connection
        $conn = null;        
        
    }

    
?>