<?php
    /**
     *  Output the donation total to a file.
     *
     *  @author Chris Hutcherson
     */
               
    //Database server and login information
    require($_SERVER['DOCUMENT_ROOT'] . '/dbLogin.php');

    try {
        $conn = new PDO($servername, $username, $password);

        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        //prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT TRUNCATE(SUM(Amount),2) FROM Transactions_2018");         
        $stmt->execute();

        $total = $stmt->fetch();
        $grandTotal = $total[0];
        
        
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/donationTotal.txt', "$".$grandTotal, LOCK_EX);
        echo "$".$grandTotal;
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }           
    
    //End the database connection
    $conn = null;
?>